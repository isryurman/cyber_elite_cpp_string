// Israel yurman
#include<iostream>

#include "MyString.h"
using namespace std;

int main()
{
	MyString my_string_1("this is base string"), my_string_2("this is second string");
	cout << "MyString object 1 was created with: " << my_string_1.getStr() << endl;
	cout << "MyString object 2 was created with: " << my_string_2.getStr() << endl;

	my_string_1.Append(&my_string_2);
	cout << "MyString object 2 was append to MyString object 1. the result are:\n" << my_string_1.getStr() << endl;
	
	system("pause");
	return 0;
}
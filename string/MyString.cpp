#include "MyString.h"



MyString::MyString()
{
	this->str_size = 0;
	this->str = NULL;
}
MyString::MyString(char* str): str(NULL)
{
	this->str_size = strlen(str) + 1;
	this->allocate();
	strcpy_s(this->str, this->str_size, str);
	this->str[str_size] = '\0';
}
MyString::~MyString()
{
	this->dallocate();
}
void MyString::allocate()
{
	if (this->str)
	{
		this->dallocate();
	}
	this->str = new char[this->str_size + 1];
}
void MyString::dallocate()
{
	if(this->str)
	{
		delete [] this->str;
		this->str = NULL;
	}
}
unsigned int MyString::GetLength()
{
	return this->str_size;
}
void MyString::Assign(const char* otherString)
{
	this->str_size = strlen(otherString) + 1;
	this->allocate();
	strcpy_s(this->str, this->str_size, str);

}
char MyString::CharAt(unsigned int index)
{
	if (index >= this->str_size)
	{
		throw "index too big\n";
	}
	
	return this->str[index];
}
void MyString::Append(const MyString *otherString)
{
	if(otherString->getStrSize() > 0)
	{
		unsigned size_1 = this->str_size, size_2 = otherString->getStrSize();
		char* temp = new char[size_1];
		strcpy_s(temp, this->str_size, this->str);

		this->str_size = this->str_size + size_2; // -1 --> need only one '\0' 

		this->allocate();
		strcpy_s(this->str, size_1, temp);
		strcat_s(this->str, size_1 + size_2, otherString->getStr());

		delete [] temp;
	}	
}
int MyString::Compare(MyString *otherString)
{
	return this->GetLength() - otherString->GetLength();
}
bool MyString::IsEmpty()
{
	return this->str_size == 0;
}
void MyString::Clear()
{
	this->dallocate();
}
int MyString::getStrSize() const
{
	return this->str_size;
}
char* MyString::getStr() const
{
	if (this->str)
	{
		return this->str;
	}
	else
	{
		return NULL;
	}
}

#pragma once
#include<string.h>

class MyString
{
private:
	char* str;
	unsigned int str_size;
	void allocate();
	void dallocate();
public:
	MyString();
	MyString::MyString(char* str);
	~MyString();
	unsigned int GetLength();
	void Assign(const char* otherString);
	char CharAt(unsigned int index);
	void Append(const MyString *otherString);
	int Compare(MyString *otherString);
	bool IsEmpty();
	void Clear();

	// getters
	int getStrSize() const;
	char* getStr() const ;
};